"use strict";

jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  $('.e-header__mob').on('click', function (e) {
    e.preventDefault();
    $('.e-header__info').slideToggle('fast');
    $('.e-header__phone').slideToggle('fast');
    $('.e-header__link').slideToggle('fast');
    $('.e-header__nav').slideToggle('fast');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.e-modal').toggle();
  });
  $('.e-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'e-modal__centered') {
      $('.e-modal').hide();
    }
  });
  $('.e-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.e-modal').hide();
  });
  $('.e-home-seo__more').on('click', function (e) {
    e.preventDefault();
    $(this).prev().toggleClass('e-home-seo_hide');
    $(this).toggleClass('active');

    if ($(this).hasClass('active')) {
      $(this).html('Скрыть');
    } else {
      $(this).html('Читать полностью');
    }
  });
  new Swiper('.e-home-swiper', {
    navigation: {
      nextEl: '.e-home-swiper .swiper-button-next',
      prevEl: '.e-home-swiper .swiper-button-prev'
    },
    spaceBetween: 600,
    loop: true
  });
  new Swiper('.e-gallery__cards', {
    navigation: {
      nextEl: '.e-gallery .swiper-button-next',
      prevEl: '.e-gallery .swiper-button-prev'
    },
    spaceBetween: 0,
    loop: true
  });
  new Swiper('.e-home-reviews__cards', {
    navigation: {
      nextEl: '.e-home-reviews__cards .swiper-button-next',
      prevEl: '.e-home-reviews__cards .swiper-button-prev'
    },
    pagination: {
      el: '.e-home-reviews__cards .swiper-pagination'
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper('.e-home-papers__cards', {
    navigation: {
      nextEl: '.e-home-papers__cards .swiper-button-next',
      prevEl: '.e-home-papers__cards .swiper-button-prev'
    },
    pagination: {
      el: '.e-home-papers__cards .swiper-pagination'
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 6
      }
    }
  });
  new Swiper('.r-features__cards', {
    navigation: {
      nextEl: '.r-features__cards .swiper-button-next',
      prevEl: '.r-features__cards .swiper-button-prev'
    },
    pagination: {
      el: '.r-features__cards .swiper-pagination'
    },
    loop: true,
    spaceBetween: 45,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 3
      }
    }
  });
});
//# sourceMappingURL=main.js.map